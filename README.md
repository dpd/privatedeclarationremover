# Private Declaration Remover #

This program is a tools that automatically remove all the private declaration from the C++ header files: private class field, method and other declaration like sub class or struct. It also remove all the comment inside and immediately above the removed codes.
This tool is very useful when you need to distribute a binary version of a library and you not want that the user see the private stuff of your classes.

Belove you can see one pace of code before and after the run of the tool.

```
#!c++

class MyClass
{
public:
    MyClass();
    ~MyClass();

    int doStuff();

protected:
    int m_data;

private:
    // Comment
    static MyClass *m_gInstance;

    // Initialize same stuff for my self only
    int doOtherStuff();
};
```

```
#!c++

class MyClass
{
public:
    MyClass();
    ~MyClass();

    int doStuff();

protected:
    int m_data;


};
```

### How do I build the program? ###

Actually the program is made for Microsoft Windows 7 or above and Visual Studio 2015, the program uses clang for parsing the headers files but you no need to build it because the binary of clang are committed in the repository.

So for build the Private Declaration Remover you need to do:

1. Download the repository
2. Open the solution "sources\PrivateDeclarationRemover.sln" with Visual Studio 2015
3. Build the debug or release configuration as your need

### Contribution guidelines ###

If you like to contribute simple:

1. Fork the repository
2. Do you change
3. Make a pull request

### Who do I talk to? ###

* Repo owner or admin