//------------------------------------------------------------------------------
// Clang rewriter sample. Demonstrates:
//
// * How to use RecursiveASTVisitor to find interesting AST nodes.
// * How to use the Rewriter API to rewrite the source code.
//
// Eli Bendersky (eliben@gmail.com)
// This code is in the public domain
//------------------------------------------------------------------------------
#include <cstdio>
#include <memory>
#include <string>
#include <fstream>

#include "clang/AST/ASTConsumer.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/AST/ASTContext.h"
#include "clang/Basic/Diagnostic.h"
#include "clang/Basic/FileManager.h"
#include "clang/Basic/SourceManager.h"
#include "clang/Basic/TargetOptions.h"
#include "clang/Basic/TargetInfo.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Lex/Preprocessor.h"
#include "clang/Parse/ParseAST.h"
#include "clang/Rewrite/Core/Rewriter.h"
#include "clang/Rewrite/Frontend/Rewriters.h"
#include "llvm/Support/Host.h"
#include "llvm/Support/raw_ostream.h"

using namespace std;
using namespace clang;

class MyASTVisitor: public RecursiveASTVisitor<MyASTVisitor>
{
    public:
        MyASTVisitor(Rewriter &r): rewriter(r)
        {
            opt.RemoveLineIfEmpty = true;
        }

        bool VisitDecl(Decl *d)
        {
            // NOTE: Function, function body inlined are inclused
            if(d->getAccess() == AccessSpecifier::AS_private)
            {
                // Search a comment immediatly before the declaration,
                // if founded will remove it

                ASTContext& ctx = d->getASTContext();
                SourceManager& sm = ctx.getSourceManager();

                const RawComment* rc = ctx.getRawCommentForDeclNoCache(d);
                if(rc)
                {
                    // Remove the comment
                    rewriter.RemoveText(rc->getSourceRange(), opt);
                }

                rewriter.RemoveText(d->getSourceRange(), opt);
            }

            return true;
        }

    private:
        Rewriter &rewriter;
        Rewriter::RewriteOptions opt;
};

class MyASTConsumer: public ASTConsumer
{
    public:
        MyASTConsumer(Rewriter &R): visitor(R) {}

        // Override the method that gets called for each parsed
        // top-level declaration.
        virtual bool HandleTopLevelDecl(DeclGroupRef dr)
        {
            for(DeclGroupRef::iterator b = dr.begin(), e = dr.end(); b != e; ++b)
            {
                // Traverse the declaration using our AST visitor.
                visitor.TraverseDecl(*b);
            }

            return true;
        }

    private:
        MyASTVisitor visitor;
};

int main(int argc, char *argv[])
{
    if(argc != 2)
    {
        string name = argv[0];

        // Extract executable name with extension
        size_t pos = name.find_last_of('\\');
        if(pos != string::npos)
        {
            name = name.substr(pos + 1);
        }

        // Extract executable name without extension
        pos = name.find_last_of('.');
        if(pos != string::npos)
        {
            name = name.substr(0, pos);
        }

        llvm::errs() << "Usage: " << name << " <filename>\n";

        return EXIT_FAILURE;
    }

    // CompilerInstance will hold the instance of the Clang compiler for us,
    // managing the various objects needed to run the compiler.
    CompilerInstance compInst;
    compInst.createDiagnostics();

    LangOptions &lo = compInst.getLangOpts();
    lo.CPlusPlus = 1;
    // Treat ordinary comments as documentation comments
    lo.CommentOpts.ParseAllComments = true;

    // Initialize target info with the default triple for our platform.
    shared_ptr<TargetOptions> to = make_shared<TargetOptions>();
    to->Triple = llvm::sys::getDefaultTargetTriple();
    TargetInfo *ti = TargetInfo::CreateTargetInfo(compInst.getDiagnostics(), to);
    compInst.setTarget(ti);

    compInst.createFileManager();
    FileManager &fileMgr = compInst.getFileManager();
    compInst.createSourceManager(fileMgr);
    SourceManager &sourceMgr = compInst.getSourceManager();
    compInst.createPreprocessor(TU_Module);
    compInst.createASTContext();

    // A Rewriter helps us manage the code rewriting task.
    Rewriter rewriter;
    rewriter.setSourceMgr(sourceMgr, compInst.getLangOpts());

    // Set the main file handled by the source manager to the input file.
    const FileEntry *fileIn = fileMgr.getFile(argv[1]);
    sourceMgr.setMainFileID(sourceMgr.createFileID(fileIn, SourceLocation(), SrcMgr::C_User));
    compInst.getDiagnosticClient().BeginSourceFile(compInst.getLangOpts(), &compInst.getPreprocessor());

    // Create an AST consumer instance which is going to get called by ParseAST.
    MyASTConsumer consumer(rewriter);

    // Parse the file to AST, registering our consumer as the AST consumer.
    ParseAST(compInst.getPreprocessor(), &consumer, compInst.getASTContext());

    // At this point the rewriter's buffer should be full with the rewritten
    // files contents.

    for(Rewriter::buffer_iterator i = rewriter.buffer_begin(); i != rewriter.buffer_end(); ++i)
    {
        const FileEntry *fe = sourceMgr.getFileEntryForID(i->first);

        ofstream f(fe->getName());
        if(f.is_open())
        {
            f << string(i->second.begin(), i->second.end());
            f.close();

            llvm::outs() << "Cleaned the file '" << fe->getName() << "'\n";
        }
        else
        {
            llvm::errs() << "ERROR: Impossible to overwrite the file '" << fe->getName() << "'\n";
        }
    }

    return EXIT_SUCCESS;
}
